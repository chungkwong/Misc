
程序组织为包的集合。每个包有自己的类型名集合，这有助于防止名称冲突。

顶级类型只当声明为public的类型时在包外可访问 (§6.6).

包的命名结构是树状的 (§7.1)。包的成员有在包的编译单元中声明的类和接口类型 (§7.6)，和可能包含自己的编译单元和子包的子包。

包可以保存于文件系统或数据库 (§7.2) 中。存储在文件系统中的软件包可能有对其编译单元的约束条件，以便实现简单地找类。

一个包包含若干编译单元 (§7.3)。编译单元自动有权访问所在包中声明的所有类型并会自动导入预定义包java.lang中声明的所有public类型.

对于小应用程序和闲暇开发，一个包可以是未命名的 (§7.4.2) 或有一个简单的名字，但如果代码是要广泛散布，惟一的包名应使用限定名。否则会发生如果两个开发组恰巧选到相同的包名而这些包后来在单个程序中使用会发生冲突。
7.1.包成员

包的成员包括它的子包和包的所有编译单元 (§7.3) 中声明的所有的顶层的类类型 （§7.6， § 8 （类）） 和顶层的接口类型。

例如，在 Java SE 平台 API:

    java包有子包awt， applet、 io、lang、net和util，但没有编译单元。

    java.awt包有名为image的子包，以及编译单元声明的若干类和接口类型。

如果包的完全限定的名称 (§6.7) 是P，Q是P的子包，那么P.Q是子包的完全限定的名，表示一个包。

一个包不能包含两个相同的名称的成员，否则会导致编译期错误。

这里有一些例子︰

    因为包java.awt有一个子包image，它不能（也并不）包含一个名为image的类或接口的类型声明.

    如果有名为mouse包和包中有成员类型Button （可被称为mouse.Button)，那么就不能有任何包的完全限定名称为mouse.Button或mouse.Button.Click.

    如果com.nighthacks.java.jag是一种类型的完全限定的名称，则不能有任何包的完全限定的名称是com.nighthacks.java.jag或com.nighthacks.java.jag.scrabble.

然而不同包的成员可以具有相同的简单名称。例如，可声明一个包︰


package vector;
public class Vector { Object[] vec; }

这里有一个public成员类名为Vector，即使java.util包还声明一个类名为Vector。它们具有不同的完全限定的名称 (§6.7) 的事实反映这两类类型是不同的。本例中Vector的完全限定的名是vector.Vector，而java.util.Vector则是 Java SE 平台中的Vector类的完全限定的名称。因为包vector包含一个名为Vector的类，它也不能有命名Vector的子包.

包的分层命名结构旨在方便以传统的方式组织相关的包，但除了子包与包中声明的顶级类型不能有相同的简单名称外本身并无意义。

例如，oliver包和oliver.twist包，或evelyn.wood包和evelyn.waugh包之间没有特殊访问权限关系。那就是，一个名为oliver.twist的包中的代码与任何其他包中的代码相比没有更多访问包oliver内声明的类型的权限。
7.2.主机支持软件包

每个主机系统确定如何创建和存储包和编译单元。

每个主机系统还确定哪些编译单元在特定的编译中是可见的 (§7.3) 。编译单元的可见性反过来确定哪些包是可见的和哪些包是在作用域内。

在 Java SE 平台的简单实现中，包和编译单元可能存储在本地文件系统。其他实现可以使用分布式的文件系统或某种形式的数据库存储他们。

如果主机系统在数据库中存储包和编译单元，则数据库必须不施加在基于文件的实现上对编译单元可选的限制。

例如，使用数据库来存储包的系统不能强制每个编译单元只有最多一个公共类或接口。

然而，使用数据库的系统必须，提供一个选项来将程序转换为服从有关限制，用于导出到基于文件的实现。

作为在文件系统中存储包的极其简单的例子，一个项目的所有包和源代码和二进制代码可能都存储在单个目录和其子目录。此目录的每个直属子目录将代表一个顶层包，那就是，一个完全限定的名只有一个单一的简单名称。每个下一级子目录将代表子包，如此例推。

目录可能包含以下直接子目录︰

com
gls
jag
java
wnj

在目录java将包含 Java SE 平台软件包;目录jag， gls和wnj可能包含三个本规范的作者供其个人使用创建，并内部互相分享; com内的包和目录会包含按用于生成唯一的名称的约定的从其它公司购买的包。

继续上例，目录java包含以下子目录︰

applet
awt
io
lang
net
util

对应于软件包java.applet， java.awt， java.io、 java.lang、 java.net和java.util被定义为 Java SE 平台 API 的一部分。

仍继续该例中，如果我们看看util目录里，我们可能会看到以下文件︰

BitSet.java        Observable.java
BitSet.class       Observable.class
Date.java          Observer.java
Date.class         Observer.class
...

在那里每个.java文件为编译单元包含类或接口定义的源，相应的.class文件载有已编译的二进制的形式 (§7.3)。

在这简单的包组织下，Java SE 平台的实现将把包名称通过放置文件名分隔符串联包名组成部分变成路径名。

例如，如果这个简单的组织被用于用‘/’作文件名分隔符的操作系统，该软件包的名称︰

jag.scrabble.board

将会变成目录名称︰

jag/scrabble/board

包名称组成部分或类名称可能包含不能正确地出现在主机文件系统普通的目录名称的字符，如在只在文件名中允许 ASCII 字符的系统上的 Unicode 字符。作为一项约定，可以由使用@字符后跟给出字符数值的四位十六进制数.

根据本约定，包名称︰

children.activities.crafts.papierM\u00e2ch\u00e9

也可以使用完整 Unicode 编写︰

children.activities.crafts.papierMâché

可能映射到目录名称︰

children/activities/crafts/papierM@00e2ch@00e9

如果@字符不是在某给定的主机文件系统的文件名称的有效字符，则在标识符中无效的其他一些字符可供使用。
7.3.编译单元

CompilationUnit是为 Java 程序的文法 (§2.3) 的目标符号 (§ 2.1)。它是如下定义︰
CompilationUnit:
[PackageDeclaration]{ImportDeclaration}{TypeDeclaration}

一个编译单元包括三个部分，其中每个是可选的︰

    package声明 (§7.4)，给出编译单元属于的包的完全限定名称。

    没有package声明的编译单元属于未命名包.

    import声明允许使用简单名引用其他包的类型和类型的static成员 (§7.5)。

    类和接口的类型的顶层声明。

每个编译单元隐式导入中预定义的包java.lang中声明的每个public类型名称，仿佛import java.lang.*;出现在每个编译单元任何package语句后。其结果是，所有这些类型的名称是可作为每个编译单元中的简单名称。

所有预定义的包java和它的子包lang和io的编译单元都是可见的.

对于所有其他包，主机系统确定哪些编译单元是可见的。

编译单元的可见性影响包的可见性.

在不同的编译单元中声明的类型可以互相循环依赖。Java 编译器必须安排在同一时间编译所有这种类型。
7.4.包声明

package声明出现在编译单元，以指示编译单元所属的包内。
7.4.1.指定的软件包

编译单元中的包声明指定编译单元的名称 (§6.2) 属于哪个包。
PackageDeclaration:
{PackageModifier}package 标识符{.标识符} ;
PackageModifier:
注解

在package声明中提到的包名称必须是包的完全限定名.

一个包最多只允许一个注解的package声明。

其中实施此限制的方式有必要取决于实现。以下为基于文件系统的实现强烈建议的方案︰ 唯一注解package声明，如果它存在，放在包含包的源文件的目录中称为package-info.java的源文件 。此文件不包含称为package-info的类的源;实际上它这样做是非法的，因为package-info不是合法的标识符。通常package-info.java只包含package声明，此之前立即是包的注解。虽然从技术上讲，此文件可能包含可包访问的一个或多个类的源代码，这是非常不好的形式。

建议package-info.java，如果它存在，代替package.html用于javadoc和其他类似的文档生成系统。如果存在此文件，则文档生成工具应该寻找package-info.java紧接 （可能注解）package声明包文档注释。在这种方式，package-info.java成为唯一包级别注解和文档仓库。如果在将来，需要添加任何其他包级别的信息，此文件应被证明是此信息的方便家。
7.4.2.未命名包

没有package声明的编译单元是未命名的包的一部分.

Java SE 平台提供未命名包主要是为了方便开发小或临时应用程序时或刚开始开发时。

未命名的包不能有子包，因为package声明的语法总是包括顶层包的名。

Java SE 平台实现必须支持至少一个未命名的包。一种实现可能支持多个未命名的包，但不是必需这样做。其中编译单元在哪个未命名包由主机系统决定。

7.4.2-1 示例。

编译单元︰

class FirstCall {
    public static void main(String[] args) {
        System.out.println("Mr. Watson, come here. "
                           + "I want you.");
    }
}

将定义未命名包的一个非常简单的编译单元。

在使用分层文件系统存储包的Java SE 平台实现，一个典型的策略是将每个目录关联天下一个未命名包;每次只有一个未命名的包可见，即与"当前的工作目录"相关联的。"当前工作目录"的确切含义取决于主机系统。
7.4.3.包的可见性

包是可见的当且仅当任一成立︰

    其中包含包声明的编译单元可见 (§7.3).

    包的子包可见。

包java、 java.lang和java.io都可见。

我们可以这从上述规则和如下编译单元可见规则看出。预定义的包java.lang声明类Object，所以Object的编译单元始终是可见的 (§7.3)。因此， java.lang包也是可见的 (第 7.4.3 节)，java包同样。此外，由于Object是可见的Object[]数组类型隐式地存在。其超接口java.io.Serializable(§10.1) 也存在，因此java.io包是可见的。
7.5.导入声明

导入声明允许命名类型或static成员用单个标识符组成的简单名称指代 (§6.2)。

不使用适当导入声明时，指代在另一个包的类型或另一种类型的static成员的唯一方法是使用完全限定的名称 (§6.7).
ImportDeclaration:
SingleTypeImportDeclaration
TypeImportOnDemandDeclaration
SingleStaticImportDeclaration
StaticImportOnDemandDeclaration

    单一类型导入声明 (§7.5.1) 导入单个命名类型，根据其规范名称 (§6.7).

    按需类型导入声明 (7.5.2) 按需导入命名类型或命名包的所有可访问类型 (§6.6) ，根据类型或包的规范名称。

    单一静态导入声明 (§7.5.3) 从一种类型导入有给定名称的所有可访问static成员，根据其规范的名称。

    按需静态导入声明 (§7.5.4)，按需导入命名类型的所有可访问static成员，根据类型的规范名称。


import声明的类型或成员通过使可用他们简单的名字仅在编译单元实际上包含import声明内。在同一个包，其他import声明在当前编译单元或在当前编译单元 （除了package声明的注释）package声明类型或成员专门由import声明引入的范围不包括其他编译单元。
7.5.1.单一类型导入声明

单一类型导入声明导入指定规范名的类型，使在编译单元的类和接口声明中可用简单名指代。
SingleTypeImportDeclaration:
import TypeName ;

TypeName必须是类类型、 接口类型、 枚举类型或注解类型的规范名称 (§6.7)。

该名称必须是限定的 (§6.5.5.2)，否则将发生编译时错误。

命名类型不可访问是一个编译时错误 (§6.6).

如果在同一编译单元中的两个单一类型导入声明试图导入具有相同的简单名称的类型，则编译时错误，除非两个类型相同，这情况重复声明忽略。

如果单一类型导入声明所导入的类型声明包含import声明的编译单元，则在import声明将被忽略。

如果单一类型导入声明导入的简单名称是n，编译单元也声明顶层类型 (§7.6) 的简单名称是n的类型，编译时错误。

如果编译单元包含导入类型的简单名称是n的单一静态导入声明和导入简单名称是n单一类型导入声明 (§7.5.1)，将发生编译时错误。

7.5.1-1 示例。单一类型导入


import java.util.Vector;

导致简单名称Vector可在编译单元中的类和接口的声明内出现。因此，简单名称Vector，在没有被域、参数、局部变量或嵌套类型声明盖过或遮盖处，指代包java.util中的类型声明Vector。

请注意， java.util.Vector的实际声明是泛型 (§8.1.2)。一旦导入，可以不加限定用在一个参数化的类型，如Vector<String>，或作为原始类型Vector使用名称Vector。import声明相关的限制是可以导入在泛型类型声明中的嵌套的类型，但其外部类型总是擦去。

7.5.1-2 示例。重复的类型声明

此程序︰


import java.util.Vector;
class Vector { Object[] vec; }

Vector的重复声明将导致编译时错误︰


import java.util.Vector;
import myVector.Vector;

myVector是包含编译单元的包︰


package myVector;
public class Vector { Object[] vec; }


7.5.1-3 示例。没有导入的包

注意，import语句不能导入的子包，导入类型。

例如，试图导入java.util ，然后使用名称util.Random来引用类型java.util.Random不行:


import java.util;
class Test { util.Random generator; }
  // incorrect: compile-time error


7.5.1-4 示例。导入也是包名称类型名称

包名和类型名称按命名约定通常不同。然而，在例子中别出心裁地命名包Vector，其中声明一个公共类的名为Mosquito:

package Vector;
public class Mosquito { int capacity; }

然后编译单元︰

package strange;
import java.util.Vector;
import Vector.Mosquito;
class Test {
    public static void main(String[] args) {
        System.out.println(new Vector().getClass());
        System.out.println(new Mosquito().getClass());
    }
}

从java.util包导入类Vector的单一类型导入声明不会阻止包名称Vector出现并被在随后import声明中正确识别。该示例将编译并产生输出︰

class java.util.Vector
class Vector.Mosquito


7.5.2.按需类型导入声明

按需类型导入声明允许命名包或类型中所有可访问类型按需导入。
TypeImportOnDemandDeclaration:
importPackageOrTypeName . * ;

PackageOrTypeName必须是包、 类类型、 接口类型、 枚举类型或注解类型的规范名称 (§6.7)。

如果PackageOrTypeName表示类型 (§6.5.4)，则该名称必须是限定的 (§6.5.5.2)，否则将发生编译时错误。

如果命名包或类型不可访问是一个编译时错误 (§6.6).

声明java.lang或当前编译单元按需类型导入声明中命名包的名称不是编译时错误。在这种情况下，按需类型导入声明将被忽略。

在同一编译单元中的两个或更多按需类型导入声明可能命名相同的类型或包。但这些声明除了一个都被认为多余的;其效果是，仿佛导入类型只有一次。

如果编译单元包含按需类型导入声明和按需静态导入声明 (§7.5.4) 名字相同的类型，其效果是好像只有一次导入 （§8.5， §9.5） 那个类型的static成员类型。

7.5.2-1 示例。按需类型导入


import java.util.*;

使java.util包中声明所有public类型，可在编译单元的类和接口声明内用简单名称。因此，在编译单元在类型声明中没有被盖过和遮盖的地方，简单名称Vector是指包java.util的Vector.

声明可能被简单名称是Vector的单一类型导入声明、在编译单元声明的属于包的类型Vector、任何嵌套的类或接口Vector。

声明可能被命名Vector的域、 参数或局部变量的声明遮盖.

（发生这些情况是不寻常的）。

7.5.3.单一静态导入声明

单一静态导入声明从一种类型导入具有给定简单名称的所有可访问static成员。使得编译单元的类和接口声明可用它们的简单名称。
SingleStaticImportDeclaration:
importstaticTypeName.标识符 ;

TypeName必须是类类型、 接口类型、 枚举类型或注解类型的规范名称 (§6.7)。

该名称必须是限定的 (§6.5.5.2)，否则发生编译时错误。

命名包或类型不可访问是一个编译时错误 

标识符必须命名命名类型的至少一个可访问static成员，否则是一个编译时错误。

允许一个单一静态导入声明要导入的几个域或类型具有相同的名称，或是具有相同名称和签名的几种方法。

如果单一静态导入声明导入的简单名称是n，类型和编译单元也声明顶层类型 (§7.6) 的简单名称是n，编译时错误。

如果编译单元包含导入类型的简单名称是n的单一静态导入声明和导入简单名称是n单一类型导入声明 (§7.5.1)，将发生编译时错误。
7.5.4.按需静态导入声明

按需静态导入声明允许按需导入命名类型的所有可访问static成员。
StaticImportOnDemandDeclaration:
import static TypeName . * ;

TypeName必须是类类型、 接口类型、 枚举类型或注解类型的规范名称 (§6.7)。

该名称必须是限定的 (§6.5.5.2)，否则发生编译时错误。

命名包或类型不可访问是一个编译时错误

在同一编译单元的两个或更多按需静态导入声明可能命名相同的类型;效果是好像只有一个此类宣言。

在同一编译单元的两个或更多按需静态导入声明可能命名相同的成员;其效果成员好像只导入一次。

允许一个按需静态导入声明要导入的多个域或类型具有相同的名称，或具有相同名称和签名的几种方法。

如果编译单元包含按需静态导入声明和对按需类型导入声明 (7.5.2) 名称相同的类型，其效果是好像只有一次导入 （§8.5， §9.5） 那个类型的static成员类型。
7.6.顶层类型声明

顶层类型声明声明一个顶层的类类型或接口类型
TypeDeclaration:
ClassDeclaration
InterfaceDeclaration
;

额外的";"标记出现在编译单元的类型声明一级对编译单元的含义没有影响。流浪的分号获准在 Java 编程语言只是作为一种到 c++ 程序员在类声明后放置";"的让步。他们不应该用于新的 Java 代码。

在没有访问修饰符时，顶级类型具有包访问︰ 它是只在被编译单元所在包中其它声明访问。可public从其他包中的代码授予访问权限的类型声明的类型。

如果顶层类型声明中包含任何一个下面的访问修饰符︰protected、private或static，是一个编译时错误.

如果接口类型中声明或顶级类型的名称显示为任何其他顶级类的名称相同的包，是一个编译时错误。

例7.6-1。冲突的顶级类型声明

package test;
import java.util.Vector;
class Point {
    int x, y;
}
interface Point {  // compile-time error #1
    int getR();
    int getTheta();
}
class Vector { Point[] pts; }  // compile-time error #2

在这里，第一个编译时错误是名称Point作为一个类和一个接口相同的包中重复声明。第二个编译时错误是试图声明名为Vector类的类型声明和单一类型导入声明冲突。

但是，要注意的是，命名一种类型与可能由所在编译单元的按需类型导入 (§7.3)导入的类声明的类同名不是错误。因此，在这个程序中︰

package test;
import java.util.*;
class Vector {}  // not a compile-time error

类Vector的声明被允许，即使也有类java.util.Vector类。在这个编译单元内的简单名称Vector是指test.Vector，不是java.util.Vector （仍然可以是由代码所在编译单元中指代，但只能用其完全限定的名称）。

例 7.6-2。顶级类型的作用域

package points;
class Point {
    int x, y;           // coordinates
    PointColor color;   // color of this point
    Point next;         // next point with this color
    static int nPoints;
}
class PointColor {
    Point first;        // first point with this color
    PointColor(int color) { this.color = color; }
    private int color;  // color components
}

此程序定义的两个类在类成员声明中使用对方。因为Point和PointColor类类型有包points的类型声明，包括所有那些在当前编译单元的，作为他们的作用域，该程序可以正确编译。那就是前, 向前引用不是问题。

7.6-3 的示例。完全限定的名称


class Point { int x, y; }

在此代码中，类Point声明于与没有package语句的编译单元中，因此Point是它的完全限定名，在代码中︰


package vista;
class Point { int x, y; }

类Point的完全限定的名是的vista.Point。（包名称vista是适合本地或个人使用; 如果包旨在广泛分布，给它一个独特的包名称更好)

Java SE 平台实现必须由其二进制名称跟踪包内的类型 (§13.1)。类型的多种的命名必须扩展为二进制名称以确保这些类名理解为指代同一类型。

例如，如果编译单元包含单一类型导入声明 (§7.5.1):


import java.util.Vector;

然后在该编译单元内Vector的简单名称和完全限定的名称java.util.Vector引用相同的类型。

当且仅当包存储在文件系统中 (§7.2)，主机系统可以选择强制编译时错误，如果类型名称加上扩展名 （如.java或.jav）的文件中找不到类型︰

    类型被所在包的其他编译单元的代码指代。

    类型声明为public的 （和因此是从其他包中的代码可以访问）。

这种限制意味着，每个编译单元最多一个这种类型。这种限制使得Java 编译器容易找到在包内的命名类。在实践中，很多程序员选择把每个类或接口类型放在自己的编译单元，不论它是否public的或由其他编译单元中的代码引用。

例如，public类型wet.sprocket.Toad的源代码会在目录wet/sprocket的Toad.java文件中，和会在相同的目录的Toad.class文件中找到相应的对象代码。
