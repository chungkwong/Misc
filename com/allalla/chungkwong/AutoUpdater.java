/* 
 * Copyright (C) 2015,2016 Chan Chung Kwong
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at
 * your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 */
package com.allalla.chungkwong;
import java.io.*;
import java.net.*;
import java.security.cert.*;
import java.util.*;
import javax.net.ssl.*;
/**
 * 自动发微博机器人（代码质量低下，不适用于生产用途）
 * @author Chan Chung Kwong <1m02math@126.com>
 */
public final class AutoUpdater{
	private static final String clientId="";//TODO: 请填真实信息
	private static final String clientSecret="";//TODO: 请填真实信息
	private static final String redirectUri="";//TODO: 请填真实信息
	private static final String UpdateUri="https://api.weibo.com/2/statuses/update.json";
	/**
	 * 发送一条微博
	 * @param status 发送内容
	 * @param token access_token
	 * @return 请求返回的JSON对象
	 */
	public static HashMap<String,Object> 发送一条微博(String status,String token)throws Exception{
		return post(UpdateUri,"status="+status+"&access_token="+token);
	}
	/**
	 * 发送微博，过长的内容分成多条微博，间隔不固定时间
	 * @param line 发送内容
	 * @param token access_token
	 */
	public static void 刷屏(String line,String token)throws Exception{
		int lastBreak=0, start=0, curr=0;
		while(curr<line.length()){
			if(!Character.isLetter(line.charAt(curr))){
				lastBreak=curr;
			}
			if(curr-start>=138||curr+1==line.length()){
				String status=null;
				if(lastBreak>start&&curr+1!=line.length()){
					status=line.substring(start,lastBreak+1);
					start=lastBreak+1;
				}else{
					status=line.substring(start,curr+1);
					start=curr+1;
				}
				发送一条微博(status,token);
				Thread.sleep((long)(60000+Math.random()*200000));
			}
			++curr;
		}
	}
	/*我不怕中间人攻击，所以信任何证书*/
	private static void trustAllHttpsCertificates() throws Exception{
		javax.net.ssl.TrustManager[] trustAllCerts=new javax.net.ssl.TrustManager[1];
		trustAllCerts[0]=new X509TrustManager(){
			@Override
			public X509Certificate[] getAcceptedIssuers(){
				return null;
			}
			@Override
			public void checkServerTrusted(X509Certificate[] arg0,String arg1)throws CertificateException{
			}
			@Override
			public void checkClientTrusted(X509Certificate[] arg0,String arg1)throws CertificateException{
			}
		};
		javax.net.ssl.SSLContext sc=javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null,trustAllCerts,null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	}
	/**
	 * 程序入口，从标准输入流获取刷屏内容
	 * @param args 第一个参数为access_token，没有的话会
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception{
		Scanner in=new Scanner(System.in);
		if(args.length!=1){
			java.awt.Desktop.getDesktop().browse(new URI("https://api.weibo.com/oauth2/authorize?client_id="+clientId+"&response_type=code&redirect_uri="+redirectUri+"&forcelogin=true"));
			System.out.println("请输入code参数");
			String code=in.next();
			System.out.println("你的access_token为"+post("https://api.weibo.com/oauth2/access_token","client_id="+clientId+"&client_secret="+clientSecret+"&grant_type=authorization_code"+"&redirect_uri="+redirectUri+"&code="+code).get("access_token"));
		}
		String accessToken=args[0];
		trustAllHttpsCertificates();
		while(in.hasNext()){
			String line=in.nextLine().trim();
			if(!line.isEmpty()){
				刷屏("[如非僵屍请取消关注]"+line,accessToken);
			}
		}
	}
	/**
	 * 发送POST请求
	 * @param url	请求目标
	 * @param parameters	欲传参数
	 * @return 返回的JSON对象
	 */
	public static HashMap<String,Object> post(String url,String parameters)throws Exception{
		URLConnection conn=new URL(url).openConnection();
		conn.setDoOutput(true);
		try(OutputStreamWriter out=new OutputStreamWriter(conn.getOutputStream())){
			out.write(parameters);
			out.flush();
		}
		return parseSimpleJSON(conn.getInputStream());
	}
	/**XXXX错误的JSON解析器，只是这里刚好够用
	 * @param in 待解析文本流
	 * @return JSON对象
	 */
	public static HashMap<String,Object> parseSimpleJSON(InputStream in)throws Exception{
		HashMap<String,Object> result=new HashMap<>();
		StreamTokenizer reader = new StreamTokenizer(new InputStreamReader(in,"UTF-8"));
		while (reader.nextToken() != StreamTokenizer.TT_EOF){
	  		reader.nextToken();
		  	String key=reader.sval;
		  	reader.nextToken();
		  	if(reader.ttype==StreamTokenizer.TT_EOF)
		  		break;
		  	if(reader.nextToken()!=StreamTokenizer.TT_NUMBER){
		  		String val=reader.sval;
		  		int lv=1;
		  		result.put(key,val);
		  	}else{
		  		double val=reader.nval;
		  		result.put(key,val);
		  	}
		}
		return result;
	}
}
